(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.helpdesk_zammad_chat = Drupal.helpdesk_zammad_chat || {};

  Drupal.behaviors.helpdesk_zammad_chat = {
    attach: function () {
      $('body').once('helpdesk-zammad-chat').each(function () {
        if (!drupalSettings.helpdesk_zammad_chat.show) {
          $(this).append('<button class="open-zammad-chat">' + drupalSettings.helpdesk_zammad_chat.label + '</button>');
        }
        new ZammadChat({
          title: drupalSettings.helpdesk_zammad_chat.label,
          fontSize: drupalSettings.helpdesk_zammad_chat.fontsize,
          chatId: drupalSettings.helpdesk_zammad_chat.id,
          show: drupalSettings.helpdesk_zammad_chat.show,
          debug: drupalSettings.helpdesk_zammad_chat.debug
        });
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
