<?php

namespace Drupal\helpdesk_zammad\Plugin\HelpdeskIntegration;

use Drupal\comment\CommentInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\helpdesk_integration\Entity\Issue;
use Drupal\helpdesk_integration\HelpdeskInterface;
use Drupal\helpdesk_integration\HelpdeskPluginException;
use Drupal\helpdesk_integration\IssueInterface;
use Drupal\helpdesk_integration\PluginBase;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use ZammadAPIClient\Client;
use ZammadAPIClient\ResourceType;

/**
 * Plugin implementation of the Zammad helpdesk.
 *
 * @HelpdeskPlugin(
 *   id = "zammad",
 *   label = @Translation("Zammad"),
 *   description = @Translation("Provides the Zammad helpdesk plugin.")
 * )
 */
class Zammad extends PluginBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function settingKeys(): array {
    return [
      'url',
      'api_token',
      'group',
      'max_file_size',
      'states',
      'state_closed',
      'chat_id',
      'chat_debug',
      'chat_show',
      'chat_label',
      'chat_fontsize',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(HelpdeskInterface $helpdesk, array $required): array {
    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('URL'),
      '#default_value' => $helpdesk->get('url'),
    ] + $required;
    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Token'),
    ] + $required;
    $form['group'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Group'),
      '#default_value' => $helpdesk->get('group'),
    ];
    $form['max_file_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Max. file size for attachments (in MB)'),
      '#min' => 1,
    ] + $required;
    $states = $helpdesk->get('url') ? $this->getStates($helpdesk) : [];
    $form['states'] = [
      '#type' => 'value',
      '#value' => $states,
    ];
    $form['state_closed'] = [
      '#type' => 'select',
      '#title' => $this->t('Closed ticket state'),
      '#options' => $states,
    ];

    $form['chat'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Chat'),
      '#weight' => 9,
    ];
    $form['chat']['chat_id'] = [
      '#type' => 'number',
      '#title' => $this->t('ID'),
      '#default_value' => $helpdesk->get('chat_id'),
      '#min' => 1,
    ];
    $form['chat']['chat_detail'] = [
      '#type' => 'container',
      '#states' => [
        'invisible' => [
          'input[name="chat_id"]' => ['value' => ''],
        ],
      ],
    ];
    $form['chat']['chat_detail']['chat_debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug mode'),
      '#default_value' => $helpdesk->get('chat_debug'),
    ];
    $form['chat']['chat_detail']['chat_show'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show automatically'),
      '#default_value' => $helpdesk->get('chat_show'),
    ];
    $form['chat']['chat_detail']['chat_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $helpdesk->get('chat_label'),
    ];
    $form['chat']['chat_detail']['chat_fontsize'] = [
      '#type' => 'number',
      '#title' => $this->t('Font size'),
      '#default_value' => $helpdesk->get('chat_fontsize'),
      '#min' => 8,
    ];

    return $form;
  }

  /**
   * Instantiate and return a Zammad client object for the given helpdesk.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity with the instance's configuration.
   * @param \Drupal\user\UserInterface|null $user
   *   The user entity on behalf of which the client should communicate.
   *
   * @return \ZammadAPIClient\Client
   *   The instantiated Zammad client.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  private function getClient(HelpdeskInterface $helpdesk, UserInterface $user = NULL): Client {
    $client = new Client([
      'url' => $helpdesk->get('url'),
      'http_token' => $helpdesk->get('api_token'),
    ]);
    if ($user !== NULL) {
      $client->setOnBehalfOfUser($this->getRemoteUserId($helpdesk, $user));
    }
    return $client;
  }

  /**
   * Get all states from the helpdesk.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity with the instance's configuration.
   *
   * @return array
   *   List of all active states indexed by their remote state ID.
   */
  private function getStates(HelpdeskInterface $helpdesk) {
    $states = [];

    try {
      if ($helpdesk->get('url')) {
        $client = $this->getClient($helpdesk);
        /** @var \ZammadAPIClient\Resource\TicketState $state */
        foreach ($client->resource(ResourceType::TICKET_STATE)->all() as $state) {
          if ($state->getValue('active')) {
            $states[$state->getID()] = $state->getValue('name');
          }
        }
      }
    }
    catch (HelpdeskPluginException $e) {
      // Ignore this exception as we may call this while not configured yet.
    }
    return $states;
  }

  /**
   * Search for the Drupal user by its remote id.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity with the instance's configuration.
   * @param string $remote_id
   *   The remote id of the user to search for.
   *
   * @return \Drupal\user\UserInterface
   *   The Drupal user entity.
   */
  private function findUserByRemoteId(HelpdeskInterface $helpdesk, string $remote_id): ?UserInterface {
    $user = $this->findUserByData($helpdesk, self::REMOTE_ID, (string) $remote_id);
    if ($user === NULL) {
      $user = User::load(1);
    }
    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function pushUser(HelpdeskInterface $helpdesk, UserInterface $user): void {
    $client = $this->getClient($helpdesk);
    /** @var \ZammadAPIClient\Resource\User $remote_user */
    if ($remote_id = $this->getUserData($user, $helpdesk, self::REMOTE_ID)) {
      $remote_user = $client->resource(ResourceType::USER)->get($remote_id);
    }
    else {
      $remote_users = $client->resource(ResourceType::USER)->search($user->getEmail());
      if (!$remote_users) {
        $remote_user = $client->resource(ResourceType::USER);
      }
      else {
        $remote_user = is_array($remote_users) ? $remote_users[0] : $remote_users;
      }
    }
    foreach ([
      'login' => $user->getDisplayName(),
      'email' => $user->getEmail(),
    ] as $key => $value) {
      if ($remote_user->getValue($key) !== $value) {
        $remote_user->setValue($key, $value);
      }
    }
    if ($remote_user->isDirty()) {
      $remote_user->save();
    }
    if ($remote_id === NULL) {
      $this->setUserData($user, $helpdesk, self::REMOTE_ID, $remote_user->getID());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createIssue(HelpdeskInterface $helpdesk, IssueInterface $issue) {
    $client = $this->getClient($helpdesk, $issue->getOwner());
    /** @var \ZammadAPIClient\Resource\Ticket $remote_issue */
    $remote_issue = $client->resource(ResourceType::TICKET);
    $remote_issue
      ->setValues([
        'title' => $issue->label(),
        'group' => $helpdesk->get('group'),
        'customer' => $this->getRemoteUserId($helpdesk, $issue->getOwner()),
        'article' => [
          'subject' => $issue->label(),
          'body' => $issue->get('body')->value,
          'content_type' => 'text/html',
          'type' => 'note',
          'internal' => FALSE,
        ],
      ])
      ->save();
    if ($remote_issue->hasError()) {
      throw new HelpdeskPluginException($remote_issue->getError());
    }
    $issue->set('extid', $remote_issue->getID());
  }

  /**
   * {@inheritdoc}
   */
  public function addCommentToIssue(HelpdeskInterface $helpdesk, IssueInterface $issue, CommentInterface $comment) {
    $client = $this->getClient($helpdesk);
    /** @var \ZammadAPIClient\Resource\TicketArticle $remote_comment */
    $remote_comment = $client->resource(ResourceType::TICKET_ARTICLE);
    $remote_comment
      ->setValues([
        'ticket_id' => $issue->get('extid')->value,
        'subject' => $comment->label(),
        'body' => $comment->get('comment_body')->value,
        'content_type' => 'text/html',
        'type' => 'note',
        'internal' => FALSE,
      ])
      ->save();
    if ($remote_comment->hasError()) {
      throw new HelpdeskPluginException($remote_comment->getError());
    }
    $comment->set('field_extid', $remote_comment->getID());
  }

  /**
   * {@inheritdoc}
   */
  public function resolveIssue(HelpdeskInterface $helpdesk, IssueInterface $issue): void {
    $closed_state = $helpdesk->get('state_closed');
    $client = $this->getClient($helpdesk, $issue->getOwner());
    /** @var \ZammadAPIClient\Resource\Ticket $remote_issue */
    $remote_issue = $client->resource(ResourceType::TICKET)->get($issue->get('extid')->value);
    $remote_issue
      ->setValue('state_id', $closed_state)
      ->save();

    $states = $helpdesk->get('states');
    $issue->set('status', $states[$closed_state] ?? '');
  }

  /**
   * {@inheritdoc}
   */
  public function getAllIssues(HelpdeskInterface $helpdesk, UserInterface $user, int $since = 0): array {
    $states = $helpdesk->get('states');
    $state_closed = (string) $helpdesk->get('state_closed');
    $client = $this->getClient($helpdesk, $user);
    $issues = [];
    if ($since > 0) {
      $date = $this->dateFormatter->format($since, 'custom', 'Y-m-d');
      $tickets = $client->resource(ResourceType::TICKET)->search('updated_at>' . $date);
    }
    else {
      $tickets = $client->resource(ResourceType::TICKET)->all();
    }
    //Fetch articles does not work with On-Behalf header in API Request
    $client->unsetOnBehalfOfUser();
    /** @var \ZammadAPIClient\Resource\Ticket $ticket */
    foreach ($tickets as $ticket) {
      /** @var \ZammadAPIClient\Resource\TicketArticle[] $articles */
      $articles = $ticket->getTicketArticles();
      $firstArticle = array_shift($articles);
      /** @var \Drupal\helpdesk_integration\IssueInterface $issue */
      $issue = Issue::create([
        'helpdesk' => $helpdesk->id(),
        'extid' => $ticket->getID(),
        'resolved' => (string) $ticket->getValue('state_id') === $state_closed,
        'title' => $ticket->getValue('title'),
        'status' => $states[$ticket->getValue('state_id')] ?? '',
        'body' => [
          'value' => empty($firstArticle) ? '' : $firstArticle->getValue('body'),
          'format' => 'basic_html',
        ],
        'created' => strtotime($ticket->getValue('created_at')),
        'changed' => strtotime($ticket->getValue('updated_at')),
      ]);
      foreach ($articles as $article) {
        $comment_id = $issue->addComment(
          $article->getID(),
          $article->getValue('body'),
          $this->findUserByRemoteId($helpdesk, $article->getValue('created_by_id')),
          strtotime($article->getValue('created_at')),
          strtotime($article->getValue('updated_at'))
        );
        // TODO: Handle attachments, see drupalspoons/helpdesk_integration#17.
        foreach ($article->getValue('attachments') as $attachment) {
          $issue->addAttachment($comment_id, $attachment['filename'], '');
        }
      }
      $issues[] = $issue;
    }
    return $issues;
  }

}
